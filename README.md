SENIORTUNES
===========

Ultra-simple, two button Raspberry-Pi based MP3 player for senior citizens.

THIS WORK IS POSTED UNDER A CREATIVE COMMONS LICENSE (for now).  This will likely change in the future.

For commercial use, contact the author.

USER INTERFACE
==============
This is a hardware player based on a Raspberry Pi.  It supports only one mode: album shuffle.  It supports either one or two skip options: press to skip album, or short-press to skip song, long-press to skip album.

There are two [large lighted buttons](http://www.digikey.com/product-search/en?FV=ffec38ed): 

1. Play/Stop - this button "breathes" green when playing, "breathes" red when stopped, and is bright when pressed.
2. Skip - this button flashes yellow when skipping, or if song/album skip mode is enabled, flashes twice when depressed long enough for album skip.

Seniortunes will take an SD card with a directory full of subdirectories which contain albums.  An "album" is a directory full of playable files.

Advanced
--------

There will be "codes" enterable with the two UI buttons to allow the following:

1. Toggle skip or skip song/album dual mode function.
2. Enable sshd
3. Unmount SD card
4. Remount SD card

When a code is entered, feedback will be given to the user with a series of flashes:

1. Command succeeded: both green, two flashes
2. Command failed: top red, bottom blue, two flashes
3. Serious problem: alternate flashing top red, bottom blue

PLAYBACK
========
Playback is via mpg123 or suitable equivalent (supporting MP3, M4A unlocked, Ogg, FLAC, maybe ALAC) through the built-in audio-out port.


LICENSE
=======

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/.
